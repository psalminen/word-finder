#!/usr/bin/env python3
# -*- coding=utf-8 -*-
"""
Find words using a set of letters provided

Originally created to solve the NYT Spelling Bee

## License
Copyright 2022 Paul Salminen <paulsalminen@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import argparse
import json
import os
from pathlib import Path
from typing import Dict, List, Set, Union

Json = Union[Dict[str, str], List[str]]
Letters = Union[str, List[str], Set[str]]


def get_dictionary(dict_file: Union[Path, str]) -> Json:
    """Read in the dictionary file
    Raises expection if the file does not exist

    + dict_file -> Path object to dictionary file
    Returns dictionary read from file
    """
    dict_file = dict_file if isinstance(dict_file, Path) else Path(dict_file)
    try:
        with dict_file.open("rt") as dict_stream:
            dictionary = json.load(dict_stream)
    except FileNotFoundError as file_err:
        raise Exception(
            f"Dictionary file does not exist! Path: {os.path.abspath(dict_file)}"
        ) from file_err
    except Exception as err:
        raise Exception("Unknown exception. Most likely malformed JSON input", err)
    return dictionary


def get_dictionary_words(dict_file: Union[Path, str]):
    """Read in a dict file and return a list of the words

    + dict_file -> Path object to dictionary file
    Returns list of words in file
    """
    dictionary = get_dictionary(dict_file)
    if isinstance(dictionary, dict):
        return list(dictionary.keys())
    elif isinstance(dictionary, list):
        return dictionary
    else:
        raise Exception("Improperly formed input file")


def find_matching_words(dictionary: List[str], key: str, letters: Letters) -> List[str]:
    """Main function to find matching words
    Prints the found words then returns them

    + dictionary: json-like object of words.
        Can be list of words, or dict with the word as the key
    + key: Key letter that must be in the word
    + letters: Set of letters to filter words be.
        Can be a list of letters or a string of all of them
    """
    letters = set(letters)
    letters.add(key)

    # This is the same as what's below, just more explicit
    # Left in to describe stuff to budding pythonistas
    # found = []
    # for word in dictionary:
    #   tmp = word.lower()
    #   if len(tmp) < 4:
    #       continue
    #   if key not in tmp:
    #       continue
    #   if set(tmp).issubset(letters):
    #       found.append(tmp)

    found = [
        x.lower()
        for x in filter(
            lambda x: len(x) >= 4
            and key in x.lower()
            and set(x.lower()).issubset(letters),
            dictionary,
        )
    ]

    pangrams = [word for word in found if set(word) == set(letters)]

    print("\n".join(pangrams), "\n=====================")

    print("\n".join(sorted(found, key=lambda x: len(x), reverse=True)))
    return found


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Find words in a dictionary using a subset of letters"
    )
    parser.add_argument(
        "letters",
        metavar="letters",
        type=str,
        help="Set of letters to filter words by, formatted as a string. Words can only contain these letters",
    )
    parser.add_argument(
        "--key",
        "-k",
        dest="key",
        required=False,
        default=None,
        help="Key letter that must exist in every word. If not given, first letter will be used",
    )
    parser.add_argument(
        "--dict",
        "-d",
        dest="dict_file",
        required=False,
        default="./dict.json",
        help="Path to dictionary file to use. Format should be json-like dict.",
    )
    args = parser.parse_args()
    key_letter = args.key or args.letters[0]
    words = get_dictionary_words(Path(args.dict_file))
    _ = find_matching_words(words, key_letter, args.letters)
